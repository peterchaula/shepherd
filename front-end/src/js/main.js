import $ from 'jquery';
import flatpickr from 'flatpickr';
import RequestFacker from './util/requestFaker';
import * as Sentry from '@sentry/browser';


window.$ = window.jQuery = $;
window.RequestFacker = RequestFacker;

require('semantic-ui-css/semantic');

$(document).ready(() => {
    //side bar
//    var sidebar = $('.ui.sidebar');

//    if (sidebar.length) {
//        sidebar.sidebar('show')
//    }
});

//plugins
$('.ui.dropdown').dropdown({
    fullTextSearch: true
});

flatpickr('.date-time-picker', {
    enableTime: true,
    dateFormat: "Y-m-d\\ H:i:S",
    "time_24hr": true
});

Sentry.init({ dsn: 'https://9f6c40df93184a55a8d172f46838b2dc@o287297.ingest.sentry.io/1521177' });
Sentry.setUser(window.user?.email);
