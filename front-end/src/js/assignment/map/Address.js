import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from '../../util/axios';
import * as Sentry from '@sentry/browser';

const addressString = (address) =>
    `${address.street}, ${address.city}, ${address.suburb}, ${address.postalCode}`

export default class Address extends Component {
    static propTypes = {
        address: PropTypes.object.isRequired,
        title: PropTypes.string.isRequired,
        onAddressUpdated: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            editing: false,
            loading: false,
            autocomplete: null
        };
        this.input = React.createRef();
    }

    setupAutoComplete = () => {
        const autocomplete = new google.maps.places.Autocomplete(this.input.current);

        autocomplete.setFields(['address_components', 'geometry']);
        autocomplete.addListener('place_changed', this.updateAddress);
        this.input.current.focus();

        this.setState({
            autocomplete
        });
    };

    updateAddress = () => {
        const place = this.state.autocomplete.getPlace();

        if (!place.geometry) {
            alert('Could not find address. Please search again');

            return;
        }

        this.setState({
            loading: true
        });

        const location = {
            lat: place.geometry.location.lat(),
            lon: place.geometry.location.lng()
        };

        axios.put(`/api/v1/company/address/${this.props.address.id}`, {_method: 'PUT', ...this.props.address, ...location})
            .then((response) => {
                this.cancelEditing();
                this.props.onAddressUpdated(response.data);
                alert('Address updated');
            })
            .catch((error) => {
                Sentry.setExtras({
                    address: this.props.address,
                    place
                });
                Sentry.captureException(error);
                alert('An error occurred while updating your address');
            })
            .finally(() => {
                this.setState({
                    loading: false
                });
            });
    };

    edit = (e) => {
        e.stopPropagation();
        this.setState(
            (state, props) => ({editing: true}),
            this.setupAutoComplete
        );
    };

    cancelEditing = (e) => {
        if (!e || e.keyCode === 27) {
            e?.stopPropagation();
            //cleanup after ourselves
            this.setState({
                editing: false,
                autocomplete: null
            });
        }
    };

    render() {
        return <div className={`ui ${this.state.loading ? 'loading' : ''} small segment`}>
            <div className="ui sub header">{this.props.title}</div>
            <div className="description"
                 onClick={(e) => e.stopPropagation()}>
                {!this.state.editing
                    ?
                    <p
                        className={this.props.address.requiresGeocoding ? 'red text' : 'green text'}
                        onClick={this.edit}>
                        {addressString(this.props.address)}
                        {this.props.address.requiresGeocoding
                            ? <i className="ui red exclamation triangle icon"
                                 title="Click to set address gps coordinates"/>
                            : ''}
                    </p>
                    :
                    <input type="text"
                           ref={this.input}
                           style={{width: '100%'}}
                           onKeyUp={this.cancelEditing}
                           defaultValue={addressString(this.props.address)}/>
                }
            </div>
        </div>
    }
}

export function addressInfoHmtl(address, title, subTitle) {
    return`
      <div class="card">
        <div class="content">
          <div class="header">${title}</div>
          <div class="meta">${subTitle}</div>
          <div class="description">
            ${addressString(address)}
          </div>
        </div>
      </div>
    `;
}
