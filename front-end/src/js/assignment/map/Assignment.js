import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Address from "./Address";

export class Assignment extends Component {
    static propTypes = {
        assignment: PropTypes.object.isRequired,
        isSelected: PropTypes.bool.isRequired,
        onAssignmentClick: PropTypes.func.isRequired,
        onAddressUpdated: PropTypes.func.isRequired,
        assignable: PropTypes.bool.isRequired,
        onAssign: PropTypes.func.isRequired,
        onUnassign: PropTypes.func.isRequired
    };

    static defaultProps = {
        assignable: false,
        onAssign: () => {
        },
        onUnassign: () => {
        }
    };

    assign = (e) => {
        e.stopPropagation();
        this.props.onAssign(this.props.assignment);
    };

    unassign = (e) => {
        e.stopPropagation();
        this.props.onUnassign(this.props.assignment);
    };

    render() {
        return (
            <div
                className={`ui ${this.props.isSelected ? 'blue' : ''} segment`}
                onClick={() => this.props.onAssignmentClick(this.props.assignment)}>
                <div className="ui small light-grey segment">
                    <div className={`ui small ${this.props.assignment.driver ? 'green' : 'grey'} header`}>
                        <a href={`/admin/company/assignment/${this.props.assignment.id}/edit`}
                           target="_blank">
                            {this.props.assignment.description}
                        </a>
                    </div>
                    <a className="mini ui labeled icon blue button"
                       href={`/admin/company/user/${this.props.assignment.driver?.id}/edit`}
                       target="_blank">
                        &nbsp;<i className="ui icon car"/>{this.props.assignment.driver?.fullName || 'Unassigned'}</a>
                    {
                        this.props.assignable
                            ?
                            <button className="mini ui labeled icon  grey button"
                                    onClick={this.assign}>
                                <i className="arrow up icon"/> Assign
                            </button>
                            :
                            <button className="mini ui labeled icon red button"
                                    onClick={this.unassign}>
                                <i className="arrow down icon"/> Unassign
                            </button>
                    }
                </div>
                <Address title="FROM"
                         address={this.props.assignment.fromAddress}
                         onAddressUpdated={this.props.onAddressUpdated}/>
                <Address title="TO"
                         address={this.props.assignment.toAddress}
                         onAddressUpdated={this.props.onAddressUpdated}/>
            </div>
        )
    }
}
