import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Dropdown} from 'semantic-ui-react';
import _ from 'lodash';

export default class UserDropdown extends Component {
    static propTypes = {
        users: PropTypes.array.isRequired,
        onChange: PropTypes.func.isRequired
    };

    static defaultProps = {
        placeholder: 'Select user',
    };

    render() {
        const options = this.props.users.map((user) => {
            return {
                key: user.id,
                text: user.fullName,
                value: user.id,
                image: {avatar: true, src: '/img/avatar.png'}
            };
        });
        return <Dropdown placeholder={this.props.placeholder}
                         disabled={this.props.disabled}
                         loading={this.props.loading}
                         fluid
                         selection
                         options={options}
                         onChange={(e, data) => this.props.onChange(_.find(this.props.users, {id: data.value}))}/>
    }
}