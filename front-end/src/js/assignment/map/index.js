import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {DragDropContext, Draggable, Droppable} from 'react-beautiful-dnd';
import {Assignment} from "./Assignment";
import axios from '../../util/axios';
import * as Sentry from '@sentry/browser';
import {Button, Icon} from 'semantic-ui-react'
import UserDropdown from './UserDropdown';
import {Directions} from './Directions';
import {addressInfoHmtl} from './Address';
import moment from 'moment';

const company = window.company;
const origin = {lat: company.lat, lng: company.lon};
const google = window.google;

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightgrey' : 'inherit',
});

const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: 'none',
    ...draggableStyle,
});

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            assignments: [],
            assigned: [],
            currentAssignment: null,
            currentDriver: null,
            drivers: [],
            infoWindow: null,
            loading: false,
            map: null,
            markers: [],
            showAssigned: false
        };
        this.map = React.createRef();
    }

    onDragEnd = (result) => {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const assigned = reorder(
            this.state.assigned,
            result.source.index,
            result.destination.index
        );

        this.setState({
            assigned,
        });
    };

    selectAssignment = (assignment) => {
        this.setState((state, props) => ({
            currentAssignment: this.state.currentAssignment?.id !== assignment.id ? assignment : null,
        }), this.renderMap);
    };

    onAddressUpdated = (address) => {
        const updateAddress = (assignment) => {
            assignment.fromAddress = assignment.fromAddress.id === address.id ? address : assignment.fromAddress;
            assignment.toAddress = assignment.toAddress.id === address.id ? address : assignment.toAddress;

            return assignment;
        };
        const assignments = this.state.assignments.map(updateAddress);
        const assigned = this.state.assigned.map(updateAddress);

        this.setState({assignments, assigned}, this.renderMap)
    };

    onUserChange = (user) => {
        this.setState({currentDriver: user}, this.getAssignments);
    };

    getAssignments = () => {
        this.setState({loading: true});

        axios.get('/api/v1/company/assignment?statuses=0,1') //pending and started
            .then((response) => {
                const assignments = response.data.filter((a) => !this.state.currentDriver || this.state.currentDriver.id !== a.driver?.id);
                const assigned = response.data.filter((a) => this.state.currentDriver && this.state.currentDriver.id === a.driver?.id)
                    .sort((a, b) => a.sequence - b.sequence);

                this.setState({assignments, assigned}, this.renderMap);
            })
            .catch((error) => {
                Sentry.setExtras({
                    currentDriver: this.state.currentDriver
                });
                Sentry.captureException(error);
            })
            .finally(() => {
                this.setState({loading: false});
            });
    };

    onAssign = (assignment) => {
        if (!this.state.currentDriver) {
            return alert('Please select a driver');
        }

        const isReassignment = assignment.driver && assignment.driver.id !== this.state.currentDriver.id;

        if (isReassignment && !confirm('Are you sure you want to reassign this job?')) {
            return;
        }

        const assignments = this.state.assignments.filter((a) => a.id !== assignment.id);
        const assigned = this.state.assigned.concat(assignment);

        this.setState({assignments, assigned}, this.renderMap);
    };

    onUnassign = (assignment) => {
        const assigned = this.state.assigned.filter((a) => a.id !== assignment.id);
        const assignments = this.state.assignments.concat(assignment);

        this.setState({assignments, assigned}, this.renderMap);
    };

    onShowAssignedChange = (e) => {
        this.setState({showAssigned: e.target.checked}, this.renderMap);
    };

    updateManifest = () => {
        this.setState({loading: true});

        axios.put(`/api/v1/company/assignment/user/${this.state.currentDriver.id}/manifest`, this.state.assigned)
            .then(this.getAssignments)
            .catch((error) => {
                Sentry.setExtras({
                    manifest: this.state.assigned,
                    driver: this.state.currentDriver,
                });
                Sentry.captureException(error);
                alert('An error occurred while updating the driver\'s manifest');
            })
            .finally(() => {
                this.setState({loading: false});
            });
    };

    plotRoute = () => {
        this.setState({
            loading: true,
            showAssigned: true
        });

        this.state.directions.render(this.state.map, this.state.assigned, origin)
            .catch((e) => {
                Sentry.captureException(e);
                alert('Error: could not find directions');
                this.renderMap();
            })
            .finally(() => {
                this.setState({loading: false});
            });
    };

    downloadRoute = () => {
        const assignmentIds = this.state.assigned.map((a) => a.id).join(',');
        const iframe = window.document.createElement('iframe');

        iframe.style.display = "none";
        iframe.src = `/admin/company/assignment/csv?id=${assignmentIds}`;

        window.document.body.appendChild(iframe)
    };

    printMap = () => {
        //todo: print maps map
    };

    showInfoWindow = (marker, content) => {
        this.state.infoWindow?.close();

        const infoWindow = new google.maps.InfoWindow({content});
        infoWindow.open(this.state.map, marker);

        this.setState({infoWindow});
    };

    renderMap = () => {
        const assignments = this.state.currentAssignment ? [this.state.currentAssignment] : [...(this.state.showAssigned ? [] : this.state.assignments), ...this.state.assigned];

        this.state.directions.clear();

        for (const marker of this.state.markers) {
            marker.setMap(null)
        }

        const markers = [];
        const bounds = new google.maps.LatLngBounds();

        for (const [i, assignment] of assignments.entries()) {
            const fromMarker = new google.maps.Marker({
                position: {
                    lat: assignment.fromAddress.lat,
                    lng: assignment.fromAddress.lon
                },
                map: this.state.map,
                title: assignment.description,
                label: {
                    text: `${i + 1}`,
                    color: '#ffffff',
                    fontSize: '10px'
                },
                draggable: true,
                address: assignment.fromAddress
            });

            const toMarker = new google.maps.Marker({
                position: {
                    lat: assignment.toAddress.lat,
                    lng: assignment.toAddress.lon
                },
                map: this.state.map,
                title: assignment.description,
                label: {
                    text: `${i + 1}`,
                    color: '#fffa63',
                    fontSize: '10px'
                },
                draggable: true,
                address: assignment.toAddress
            });

            const fromSubTitle = moment(assignment.startingOn).format('LLL');
            const fromInfo = addressInfoHmtl(assignment.fromAddress, 'From:', fromSubTitle);

            const toSubTitle = moment(assignment.endingOn).format('LLL');
            const toInfo = addressInfoHmtl(assignment.toAddress, 'To:', toSubTitle);


            fromMarker.addListener('click', () => this.showInfoWindow(fromMarker, fromInfo));
            toMarker.addListener('click', () => this.showInfoWindow(toMarker, toInfo));

            markers.push(fromMarker, toMarker);
            bounds.extend(fromMarker.getPosition());
            bounds.extend(toMarker.getPosition());
        }

        this.state.map.fitBounds(bounds);
        this.setState({markers});
    };

    componentDidMount() {
        const map = new google.maps.Map(this.map.current, {
            center: origin,
            zoom: 8
        });

        this.setState({map, directions: new Directions()}, this.getAssignments);

        axios.get('/api/v1/company/user')
            .then((response) => {
                const drivers = response.data;

                this.setState({drivers});
            })
            .catch((error) => {
                Sentry.captureException(error);
                alert('Failed to load driver list. Please reload page');
            })
            .finally(() => {
                this.setState({loading: false});
            });
    }

    render() {
        return (
            <div className="ui grid">
                <div className="four wide column">
                    <div className="ui segment">
                        <UserDropdown
                            disabled={this.state.loading}
                            onChange={this.onUserChange}
                            users={this.state.drivers}
                            loading={this.state.loading}
                            placeholder="Select driver"/>
                    </div>

                    <div className="ui form">
                        <div className="ui right aligned field">
                            <Button.Group widths="5" size="mini">
                                <Button size="mini"
                                        fluid
                                        color="grey"
                                        animated="fade"
                                        loading={this.state.loading}
                                        disabled={!this.state.currentDriver || this.state.loading}
                                        onClick={this.downloadRoute}>
                                    <Button.Content visible><Icon name="download"/></Button.Content>
                                    <Button.Content hidden>Export</Button.Content>
                                </Button>
                                <Button size="mini"
                                        fluid
                                        secondary
                                        animated="fade"
                                        loading={this.state.loading}
                                        disabled={!this.state.currentDriver || this.state.loading}
                                        onClick={this.plotRoute}>
                                    <Button.Content visible><Icon name="road"/></Button.Content>
                                    <Button.Content hidden>Route</Button.Content>
                                </Button>
                                <Button size="mini"
                                        primary
                                        fluid
                                        animated="fade"
                                        loading={this.state.loading}
                                        disabled={!this.state.currentDriver || this.state.loading}
                                        onClick={this.printMap}>
                                    <Button.Content visible><Icon name="print"/></Button.Content>
                                    <Button.Content hidden>Print</Button.Content>
                                </Button>
                                <Button size="mini"
                                        fluid
                                        color="green"
                                        animated="fade"
                                        loading={this.state.loading}
                                        disabled={!this.state.currentDriver || this.state.loading}
                                        onClick={this.updateManifest}>
                                    <Button.Content visible><Icon name="save"/></Button.Content>
                                    <Button.Content hidden>Save</Button.Content>
                                </Button>
                            </Button.Group>

                        </div>
                        <div className="ui right aligned field">
                            <div className="ui checkbox" title="Only show assignments assigned to this driver">
                                <input type="checkbox"
                                       checked={this.state.showAssigned}
                                       onChange={this.onShowAssignedChange}/>
                                <label>Only show assigned</label>
                            </div>
                        </div>
                        <br/>
                    </div>
                    {!this.state.currentDriver &&
                    <h4 className="ui grey center aligned header">
                        Select a driver from the list to assign
                    </h4>
                    }
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Droppable droppableId="droppable">
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    style={getListStyle(snapshot.isDraggingOver)}>
                                    {this.state.assigned.map((assignment, index) => (
                                        <Draggable key={assignment.id} draggableId={assignment.id + ''}
                                                   index={index}>
                                            {(provided, snapshot) => (
                                                <div
                                                    ref={provided.innerRef}
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                    style={getItemStyle(
                                                        snapshot.isDragging,
                                                        provided.draggableProps.style
                                                    )}>
                                                    <Assignment assignment={assignment}
                                                                isSelected={this.state.currentAssignment?.id === assignment.id}
                                                                provided={provided}
                                                                snapshot={snapshot}
                                                                onAssignmentClick={this.selectAssignment}
                                                                onAddressUpdated={this.onAddressUpdated}
                                                                onUnassign={this.onUnassign}/>
                                                    <br/>
                                                </div>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
                <div className="ten wide column">
                    <div ref={this.map} style={{height: '720px', width: '100%'}}>
                    </div>
                    <h4 className="ui horizontal divider header">
                        <i className="warehouse icon"/> Incomplete assignments
                    </h4>
                    <div className="ui basic segment">
                        <div className="ui grid">
                            {this.state.assignments.map((assignment) => {
                                return (
                                    <div className="ui seven wide column">
                                        <Assignment key={assignment.id}
                                                    assignment={assignment}
                                                    isSelected={this.state.currentAssignment?.id === assignment.id}
                                                    onAssignmentClick={this.selectAssignment.bind(this)}
                                                    onAddressUpdated={this.onAddressUpdated}
                                                    assignable
                                                    onAssign={this.onAssign}/>
                                        <br/>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('app'));
