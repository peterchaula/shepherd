const google = window.google;

export default Directions;

export class Directions {
    constructor() {
        this.directionsService = new google.maps.DirectionsService();
        this.directionsRenderer = new google.maps.DirectionsRenderer();
        this.directionsRenderer.setOptions({
            markerOptions: {
                icon: '/img/teardrop.png'
            }
        });
    }

    render(map, assignments, origin) {
        return new Promise(((resolve, reject) => {
            const waypoints = this.getWaypoints(assignments);
            const params = {
                origin: `${origin.lat},${origin.lng}`,
                destination: `${origin.lat},${origin.lng}`,
                waypoints: waypoints.map((location) => ({location})),
                travelMode: 'DRIVING'
            };

            this.directionsRenderer.setMap(map);
            this.directionsService.route(params, (result, status) => {
                if (status === 'OK') {
                    this.directionsRenderer.setDirections(result);

                    return resolve(result);
                }

                reject(result);
            });
        }));
    }

    getWaypoints(assignments) {
        const waypoints = [];

        for (const i in assignments) {
            const assignment = assignments[i];
            const prevFrom = i > 0 ? assignments[i - 1].fromAddress : null;
            const prevTo = i > 0 ? assignments[i - 1].toAddress : null;
            const curFrom = assignment.fromAddress;
            const curTo = assignment.fromAddress;

            const latTo = assignment.toAddress.lat;
            const lngTo = assignment.toAddress.lon;

            if (prevFrom?.id !== curFrom.id) {
                const latFrom = assignment.fromAddress.lat;
                const lngFrom = assignment.fromAddress.lon;

                waypoints.push({
                    lat: latFrom,
                    lng: lngFrom
                });
            }

            if (prevTo?.id !== curTo.id) {
                waypoints.push({
                    lat: latTo,
                    lng: lngTo
                });
            }
        }

        //todo: deal with 20 waypoint limit
        return waypoints;
    }

    clear() {
        this.directionsRenderer.setMap(null);
    }
}
