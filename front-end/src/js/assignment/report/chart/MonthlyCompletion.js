import React, {PureComponent} from 'react';
import axios from '../../../util/axios';
import * as Sentry from '@sentry/browser';
import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,
} from 'recharts';
import moment from 'moment';

export default class WeeklyCompletion extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            periods: []
        };
    }

    componentDidMount() {
        Sentry.addBreadcrumb({message: "Getting monthly report periods", data: this.state})

        this.setState({loading: true});

        axios.get('/api/v1/company/assignment/report/monthly')
            .then((response) => {
                const periods = [];

                for (const period in response.data) {
                    periods.push({
                        day: moment(period).format('D'),
                        date: period,
                        completed: response.data[period]
                    });
                }

                this.setState({periods})
            })
            .catch((error) => {
                Sentry.captureException(error);
            })
            .finally(() => {
                this.setState({loading: true});
            })
    }

    render = () => {
        return (
            <div style={{width: '100%', height: 300}}>
                <ResponsiveContainer>
                    <BarChart
                        data={this.state.periods}
                        margin={{
                            top: 30, right: 0, left: 0, bottom: 5,
                        }}>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="day"/>
                        <YAxis/>
                        <Tooltip/>
                        <Legend/>
                        <Bar dataKey="completed" fill="#62b527"/>
                    </BarChart>
                </ResponsiveContainer>
            </div>
        );
    }
}
