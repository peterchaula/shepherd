import $ from 'jquery';

class RequestFaker {
    static fake(event, method = null) {
        event.preventDefault();

        let element = $(event.currentTarget);
        let url = element.attr('href');
        let prompt = element.data('prompt');
        let form = $('#requestFakerForm');

        if (!method){
            method = element.data('method');
        }

        if (!confirm(prompt)) {
            return;
        }

        form.attr('action', url);
        form.find('input[name=_method]').val(method);
        form.submit();
    }

    static patch(event) {
        RequestFaker.fake(event, 'PATCH')
    }

    static put(event) {
        RequestFaker.fake(event, 'PUT')
    }

    static delete(event) {
        RequestFaker.fake(event, 'DELETE')
    }
}

export default RequestFaker
