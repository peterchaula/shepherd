import React from 'react';
import ReactDOM from 'react-dom';
import WeeklyCompletion from '../assignment/report/chart/WeeklyCompletion';
import MonthlyCompletion from "../assignment/report/chart/MonthlyCompletion";
import AnnualCompletion from "../assignment/report/chart/AnnualCompletion";

ReactDOM.render(<WeeklyCompletion/>, document.getElementById('weekly_report'));
ReactDOM.render(<MonthlyCompletion/>, document.getElementById('monthly_report'));
ReactDOM.render(<AnnualCompletion/>, document.getElementById('annual_report'));
