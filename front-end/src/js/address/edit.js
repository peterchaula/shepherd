import * as Sentry from '@sentry/browser';

const $ = window.$;

const google = window.google;

const street = $('[name=street]');
const province = $('[name=province]');
const city = $('[name=city]');
const suburb = $('[name=suburb]');
const postalCode = $('[name=postalCode]');
const lat = $('[name=lat]');
const lon = $('[name=lon]');


const autocomplete = new google.maps.places.Autocomplete(street[0]);

Sentry.addBreadcrumb({message: 'Initialize places autocomplete'});

autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
autocomplete.addListener('place_changed', () => {
    const place = autocomplete.getPlace();

    console.log(place);

    if (!place.geometry || !place.address_components) {
        Sentry.setExtras({place});
        Sentry.captureException(new Error('Invalid place. Missing component(s)'));

        return
    }

    lat.val(place.geometry.location.lat());
    lon.val(place.geometry.location.lng());

    let streetNum = '';
    let streetName = '';
    //todo: country

    for (const component of place.address_components) {
        if (component.types.find(t => t === 'street_number')) {
            streetNum = component.short_name;
            continue;
        }

        if (component.types.find(t => t === 'route')) {
            streetName = component.short_name;
            continue;
        }

        if (component.types.find(t => t === 'sublocality')) {
            suburb.val(component.short_name);
            continue;
        }

        if (component.types.find(t => t === 'locality')) {
            city.val(component.short_name);
            continue;
        }

        if (component.types.find(t => t === 'postal_code')) {
            postalCode.val(component.short_name);
        }

        if (component.types.find(t => t === 'administrative_area_level_1')) {
            province.dropdown('set selected', component.short_name);
        }
    }

    let streetAddress = streetNum;

    if (streetName) {
        streetAddress += ` ${streetName}`;
    }

    if (streetAddress && place.name !== streetAddress) {
        streetAddress = `${place.name}, ${streetAddress}`;
    } else  {
        streetAddress = place.name;
    }

    street.val(streetAddress);
});

