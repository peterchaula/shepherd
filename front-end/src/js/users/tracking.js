import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {DragDropContext, Draggable, Droppable} from 'react-beautiful-dnd';
import {Assignment} from "../assignment/map/Assignment";
import axios from '../util/axios';
import * as Sentry from '@sentry/browser';
import {Button, Icon} from 'semantic-ui-react'
import UserDropdown from '../assignment/map/UserDropdown';
import {Directions} from '../assignment/map/Directions';

const company = window.company;
const origin = {lat: company.lat, lng: company.lon};
const google = window.google;

class Map extends Component {
    constructor(props) {
        super(props);

        this.state = {
            drivers: [],
            infoWindow: null,
            loading: true,
            map: null,
            markers: [],
        };
        this.map = React.createRef();
    }

    userInfoCard = (fullName) =>  {
        return`
      <div class="card">
        <div class="content">
          <div class="description">
            ${fullName}
          </div>
        </div>
      </div>
    `;
    }

    showInfoWindow = (marker, content) => {
        this.state.infoWindow?.close();

        const infoWindow = new google.maps.InfoWindow({content});
        infoWindow.open(this.state.map, marker);

        this.setState({infoWindow});
    };


    renderMap = () => {

        const locations = this.state.drivers ? this.state.drivers : []

        for (const marker of this.state.markers) {
            marker.setMap(null)
        }

        const markers = [];
        const bounds = new google.maps.LatLngBounds();

        for (const [i, user] of locations.entries()) {

            const marker = new google.maps.Marker({
                position: {
                    lat: user.latitude,
                    lng: user.longitude
                },
                map: this.state.map,
                title: user.fullName,
                label: {
                    text: `${user.firstName[0]} ${user.lastName[0]}`,
                    color: '#ffffff',
                    fontSize: '10px'
                },
                draggable: false
            });

            const fromInfo = this.userInfoCard(user.fullName);


            marker.addListener('click', () => this.showInfoWindow(marker, fromInfo));

            markers.push(marker);
            bounds.extend(marker.getPosition());
        }

        this.state.map.fitBounds(bounds);
        this.setState({markers});
    };

    componentDidMount() {
        const map = new google.maps.Map(this.map.current, {
            center: origin,
            zoom: 8
        });

        this.setState({map});

        axios.get('/api/v1/company/user')
            .then((response) => {
                const drivers = response.data;

                this.setState({drivers});
                this.renderMap();
            })
            .catch((error) => {
                Sentry.captureException(error);
                alert('Failed to load driver list. Please reload page');
            })
            .finally(() => {
                this.setState({loading: false});
            });
    }

    render() {
        return (
            <div className="ui grid">
                <div className="twelve wide column">
                    <div ref={this.map} style={{height: '720px', width: '100%'}}></div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<Map/>, document.getElementById('app'));
