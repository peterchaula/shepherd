const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const dst = path.resolve(__dirname, './dist/');

module.exports = {
    mode: 'development',
    entry: {
        "main": ['./src/js/main.js', './src/scss/main.scss'],
        "website": ['./src/js/website.js', './src/scss/website.css'],
        "assignment/map": ['./src/js/assignment/map/index.js'],
        "address/edit": ['./src/js/address/edit.js'],
        "company/dashboard": ['./src/js/company/dashboard.js'],
        "users/tracking": ['./src/js/users/tracking.js'],
    },
    devtool: 'source-map',
    output: {
        path: dst,
        filename: 'js/[name].js',
    },
    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: './src/img', to: `${dst}/img`},
        ]),
        new MiniCssExtractPlugin({
            path: dst,
            filename: "css/[name].css",
            chunkFilename: "css/[id].css"
        })
    ],
    module: {
        rules: [{
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: 'babel-loader',
        },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '/'
                        }
                    },
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[ext]'
                        }
                    },
                ]
            },
            {
                test: /\.(woff|woff2|ttf|eot)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: 'fonts/[name].[ext]',
                        }
                    },
                ]
            }
        ]
    }
};
