FROM openjdk:8-jre-alpine
COPY server/build/libs/shepherd-1.0.0.jar .
COPY server/version.txt .
VOLUME /tmp
EXPOSE 8080/tcp

ENTRYPOINT ["java","-jar", "-Duser.timezone=CAT", "shepherd-1.0.0.jar"]
