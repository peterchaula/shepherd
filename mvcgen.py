#!/usr/bin/python

import sys


def camelize(s):
    return s[:1].lower() + s[1:]


help = """
Generates ModelService and ModelRepository given Model

Usage: python3 model.py [Model]

"""
service_template = """
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.[model];
import io.github.peterchaula.shepherd.data.repository.[repository];
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class [service] {
    private final [repository] [repository_name];

    public [service](@Autowired  [repository] [repository_name]){
        this.[repository_name] = [repository_name];
    }

    public void save([model] [model_name]){
        this.[repository_name].save([model_name]);
    }
}
"""

repository_template = """
package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.[model];
import org.springframework.data.repository.CrudRepository;

public interface [repository] extends CrudRepository<[model], Integer> {
}
"""

controller_template = """
package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.data.entity.[model];
import io.github.peterchaula.shepherd.service.[service];
import io.github.peterchaula.shepherd.session.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/company/[model_name]")
public class [controller] extends io.github.peterchaula.shepherd.controller.Controller {

    private [service] [service_name];

    public [controller](@Autowired [service] [service_name]) {
        this.[service_name] = [service_name];
    }

    @GetMapping
    public String index(Model model) {
        return "admin/[model_name]/index";
    }

    @PostMapping
    public String store(@Valid [model] [model_name], BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("[model_name]", [model_name]);

            return "admin/[model_name]/create";
        }

        [model_name].setCompany(getAuthUser().getCompany());
        [model_name].setUser(getAuthUser());
        [service_name].save([model_name]);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "[model_name] successfully created!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/[model_name]";
    }
}
"""

model = sys.argv[1:]

if not model:
    print('Missing [Model] argument')
    print help
    sys.exit(1)

model = model[0]

base_path = 'server/src/main/java/'

model_name = camelize(model)

service = model + 'Service'
service_name = camelize(service)

repository = model + 'Repository'
repository_name = camelize(repository)

controller = model + 'Controller'
controller_name = camelize(controller)

############################################## service  ########################################################

service_path = base_path + 'io.github.peterchaula.shepherd.service.'.replace('.', '/') + service + '.java'

print('Creating service: ' + service_path)

service_file = open(service_path, 'w')
service_code = service_template.replace('[service]', service) \
    .replace('[repository]', repository) \
    .replace('[repository_name]', repository_name) \
    .replace('[model]', model) \
    .replace('[model_name]', model_name)

service_file.write(service_code)
service_file.flush()
service_file.close()

print('Service: ' + service_path + " created")

############################################## repo  ########################################################

repository_path = base_path + 'io.github.peterchaula.shepherd.data.repository.'.replace('.', '/') + repository + '.java'

print('Creating repository: ' + repository_path)

repository_file = open(repository_path, 'w')
repository_code = repository_template.replace('[repository]', repository) \
    .replace('[model]', model)

repository_file.write(repository_code)
repository_file.flush()
repository_file.close()

print('Repository: ' + repository_path + ' created')

############################################## controller  ########################################################

controller_path = base_path + 'io.github.peterchaula.shepherd.controller.admin.'.replace('.', '/') + controller + '.java'

print('Creating controller: ' + controller_path)

controller_file = open(controller_path, 'w')
controller_code = controller_template.replace('[controller]', controller) \
    .replace('[model]', model) \
    .replace('[model_name]', model_name) \
    .replace('[service]', service) \
    .replace('[service]', service) \
    .replace('[service_name]', service_name) \
    .replace('[repository]', repository) \
    .replace('[repository_name]', repository_name) \

controller_file.write(controller_code)
controller_file.flush()
controller_file.close()
print('Service: ' + service_path + " created")

print('Done!')
sys.exit(0)
