#todo: clone master branch and deploy it
VERSION=$(cat ./server/version.txt)

echo  "Version ${VERSION}"
CURR_CONTEXT=$(kubectl config current-context)

IMAGE="registry.sheps.co.za/shepherd:${VERSION}"
echo "Image: ${IMAGE}"

echo  "Switching kubectl config context from ${CURR_CONTEXT}"

CONTEXT=$1
echo  "Switching kubectl config context to ${CONTEXT}"
kubectl config use-context "${CONTEXT}"

echo "Running npm install and npm build"
npm install --prefix=front-end || exit  1
npm run prod --prefix=front-end || exit 1

echo "Running gradle jar build"
./gradlew bootJar

echo "Building docker image: ${IMAGE}"
docker build --tag "${IMAGE}" .

echo "Pushing docker image to registry: ${IMAGE}"
docker push "${IMAGE}"

echo "Applying storage pvc"
kubectl apply -f k8s/storage-pvc.yaml

echo "Applying deployment"
sed -e "s|registry.sheps.co.za/shepherd:latest|$IMAGE|g" < k8s/deployment.yaml | kubectl apply  --record -f -

echo "Applying service"
#kubectl apply -f k8s/service.yaml

#restore context
echo  "Restoring kubectl config context to ${CURR_CONTEXT}"
kubectl config use-context "${CURR_CONTEXT}"

echo "Done"
