package io.github.peterchaula.shepherd.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@Entity
@JsonIgnoreProperties({"user", "company"})
public class VehicleType extends BaseEntity {
    @Size(min=2, max=50)
    private String name;

    @Size(min=3, max=50)
    private String description;
}
