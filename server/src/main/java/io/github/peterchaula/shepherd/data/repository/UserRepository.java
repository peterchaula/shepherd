package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);

    List<User> findByCompanyId(int id);
}
