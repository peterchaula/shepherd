package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.Assignment.Status;
import io.github.peterchaula.shepherd.service.AssignmentService;
import io.github.peterchaula.shepherd.service.VehicleService;
import io.github.peterchaula.shepherd.session.FlashMessage;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/admin/company/assignment")
public class AssignmentController extends AdminController {
    private final AssignmentService assignmentService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @GetMapping
    public String index(@RequestParam(required = false) List<Integer> statuses,
                        @RequestParam(required = false, defaultValue = "") String search,
                        Model model,
                        Pageable pageable) {
        final List<Integer> defaultStatuses = Stream.of(Status.PENDING, Status.STARTED)
                .map(Status::ordinal)
                .collect(Collectors.toList());
        final List<Integer> selectedStatuses = Optional.ofNullable(statuses).orElse(defaultStatuses);
        final List<Assignment> assignments = assignmentService.search(
                getAuthUser().getCompany(),
                selectedStatuses,
                search,
                pageable
        );

        model.addAttribute("assignments", assignments)
                .addAttribute("statusList", Status.values())
                .addAttribute("selectedStatuses", selectedStatuses)
                .addAttribute("pageable", pageable);

        return "admin/assignment/index";
    }

    @GetMapping("create")
    public String create(Model model) {
        model.addAttribute("assignment", new Assignment());

        return "admin/assignment/create";
    }

    @PostMapping
    public String store(@Valid Assignment assignment, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("assignment", assignment);

            return "admin/assignment/create";
        }

        assignment.setUser(getAuthUser());
        assignment.setCompany(getAuthUser().getCompany());
        assignmentService.save(assignment);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Assignment successfully created successfully created!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/assignment";
    }

    @PatchMapping("{id}/start")
    public String start(@PathVariable int id, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        Assignment assignment = assignmentService.findByIdIn(id);
        assignmentService.start(assignment, getAuthUser());
        assignmentService.save(assignment);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Assignment successfully started!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:" + request.getHeader(HttpHeaders.REFERER);
    }

    @PatchMapping("{id}/complete")
    public String complete(@PathVariable int id, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        Assignment assignment = assignmentService.findByIdIn(id);
        assignmentService.complete(assignment, getAuthUser());
        assignmentService.save(assignment);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Assignment successfully completed!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:" + request.getHeader(HttpHeaders.REFERER);
    }

    @PatchMapping("{id}/cancel")
    public String cancel(@PathVariable int id, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        Assignment assignment = assignmentService.findByIdIn(id);
        assignmentService.cancel(assignment, getAuthUser());
        assignmentService.save(assignment);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Assignment successfully cancelled!",
                FlashMessage.STATUS.INFO
        ));

        return "redirect:" + request.getHeader(HttpHeaders.REFERER);
    }

    @GetMapping("map")
    public String map(Model model, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        model.addAttribute("assignments", assignmentService.findAll(getAuthUser().getCompany(), null));

        return "admin/assignment/map";
    }

    @GetMapping("csv")
    public ResponseEntity<StreamingResponseBody> csv(@RequestParam("id") List<Integer> assignmentIds) {
        final List<Assignment> assignments = assignmentService.findByIdIn(assignmentIds)
                .stream().sorted(Comparator.comparingInt(Assignment::getSequence)).collect(Collectors.toList());
        final String fileName = LocalDateTime.now().toString().replaceAll("\\D", "");
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.parseMediaType("text/csv"));
        headers.setContentDisposition(ContentDisposition.builder("attachment").filename("Assignments_" + fileName + ".csv").build());


        final StreamingResponseBody stream = out -> {
            final OutputStreamWriter writer = new OutputStreamWriter(out);
            final CSVPrinter printer = CSVFormat.DEFAULT
                    .withHeader("#", "No.", "Reference", "Company", "Description", "Status", "Driver", "From", "To", "Starting(est.)", "Ending(est.)")
                    .print(writer);

            for (Assignment assignment : assignments) {
                printer.printRecord(
                        assignments.indexOf(assignment) + 1,
                        assignment.getId(),
                        assignment.getReference(),
                        assignment.getCompany().getName(),
                        assignment.getDescription(),
                        assignment.getStatus(),
                        assignment.getDriver() != null ? assignment.getDriver().getFullName() : "Unassigned",
                        assignment.getFromAddress(),
                        assignment.getToAddress(),
                        assignment.getStartingOn(),
                        assignment.getEndingOn()
                );
            }

            writer.flush();
        };

        return new ResponseEntity<>(stream, headers, HttpStatus.OK);
    }
}
