package io.github.peterchaula.shepherd.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;

@Configuration
public class SharedVarsConfig {
    @Bean
    public Calendar systemTime() {
        return Calendar.getInstance();
    }
}
