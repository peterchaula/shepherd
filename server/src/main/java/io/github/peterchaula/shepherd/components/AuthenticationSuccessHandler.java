package io.github.peterchaula.shepherd.components;

import com.auth0.jwt.JWT;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

@Component
public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    final UserService userService;

    @Value("auth.jwt.secret")
    private String jwtSecret;

    public AuthenticationSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        if (authentication.getPrincipal() instanceof UserDetails) {
            userService.updateLastLogin((UserDetails)authentication.getPrincipal());

            request.getSession().setAttribute("jwtToken", JWT.create()
                    .withSubject(((User) authentication.getPrincipal()).getUsername())
                    .withExpiresAt(new Date(System.currentTimeMillis() + 8640000L * 365L)) // 1yr
                    .sign(HMAC512(jwtSecret)));
            response.sendRedirect("/admin");

            return;
        }

        throw new RuntimeException("Principal not an instance of UserDetails");
    }
}
