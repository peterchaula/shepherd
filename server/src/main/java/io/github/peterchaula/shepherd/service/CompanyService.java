package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {
    private CompanyRepository companyRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public void save(Company company) {
        this.companyRepository.save(company);
    }
}
