package io.github.peterchaula.shepherd.components;

import com.auth0.jwt.JWT;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private String secret;

    public JWTAuthorizationFilter(AuthenticationManager authManager, String secret) {
        super(authManager);
        this.secret = secret;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        final UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        if (authentication == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unable to verify jwt token");
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return !request.getRequestURI().startsWith("/api");
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        try {

            if (token != null) {
                // parse the token.
                String user = JWT.require(HMAC512(secret))
                        .build()
                        .verify(token.replaceAll("(Bearer|\\s+)", ""))
                        .getSubject();

                if (user != null) {
                    return new UsernamePasswordAuthenticationToken(User.builder()
                            .username(user)
                            .password("")
                            .roles("ADMIN")
                            .accountExpired(false)
                            .credentialsExpired(false)
                            .build(),
                            null,
                            null
                    );
                }
                return null;
            }
        } catch (RuntimeException exception) {
            logger.error("JWT verification failed", exception);
        }
        return null;
    }
}
