package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.AssignmentType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AssignmentTypeRepository extends CrudRepository<AssignmentType, Integer> {
    List<AssignmentType> findByCompanyId(int id);
}
