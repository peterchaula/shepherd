package io.github.peterchaula.shepherd.data.entity;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
public class Address extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "province_code", nullable = false)
    @NotNull
    private Province province;

    @NotNull
    @NotEmpty
    @Length(min = 3)
    private String street;

    @NotNull
    @NotEmpty
    @Length(min = 3)
    private String city;

    @NotNull
    @NotEmpty
    @Length(min = 3)
    private String suburb;

    @NotEmpty
    @Column(nullable = false)
    private String postalCode;

    @Column(nullable = false)
    private float lat;

    @Column(nullable = false)
    private float lon;

    @JsonGetter("description")
    public String toString() {
        return String.format("%s, %s, %s, %s, %s, %s", street, suburb, city, city, postalCode, province.getCode());
    }

    @JsonSerialize
    public boolean requiresGeocoding() {
        return this.lat == 0.0 || this.lon == 0.0;
    }
}
