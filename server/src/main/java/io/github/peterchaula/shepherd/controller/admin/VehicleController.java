package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.data.entity.Vehicle;
import io.github.peterchaula.shepherd.service.VehicleService;
import io.github.peterchaula.shepherd.session.FlashMessage;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Transactional
@RequestMapping("admin/company/vehicle")
public class VehicleController extends AdminController {
    private VehicleService vehicleService;

    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping
    public String index(Model model, Pageable pageable) {
        model.addAttribute("vehicles", vehicleService.findByCompany(getAuthUser().getCompany(), pageable))
        .addAttribute("pageable", pageable);


        return "admin/vehicle/index";
    }

    @GetMapping("create")
    public String create(Model model) {
        //initialize lazy relations while session's still open
        getAuthUser().getCompany().getVehicles();
        getAuthUser().getCompany().getVehicleTypes();

        model.addAttribute("vehicle", new Vehicle());

        return "admin/vehicle/create";
    }

    @PostMapping
    public String store(@Valid Vehicle vehicle, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (!bindingResult.hasErrors()) {
            if (!vehicle.getType().getCompany().equals(getAuthUser().getCompany())) {
                bindingResult.rejectValue("type", "Invalid vehicle type!");
            }
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("vehicle", vehicle);

            return "admin/vehicle/create";
        }

        final User authUser = getAuthUser();

        vehicle.setUser(authUser);
        vehicle.setCompany(authUser.getCompany());

        vehicleService.save(vehicle);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "You have successfully added a new vehicle!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/vehicle";
    }

    @PutMapping
    public String update(@Valid Vehicle vehicle, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (!bindingResult.hasErrors()) {
            if (!vehicle.getType().getCompany().equals(getAuthUser().getCompany())) {
                bindingResult.rejectValue("type", "Invalid vehicle type!");
            }
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("vehicle", vehicle);

            return "admin/vehicle/create";
        }

        final User authUser = getAuthUser();

        vehicle.setUser(authUser);
        vehicle.setCompany(authUser.getCompany());

        vehicleService.save(vehicle);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "You have successfully added a new vehicle!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/vehicle";
    }
}
