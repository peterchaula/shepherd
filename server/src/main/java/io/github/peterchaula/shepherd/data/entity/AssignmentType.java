package io.github.peterchaula.shepherd.data.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@ToString
@Entity
public class AssignmentType extends BaseEntity {
    @Length(min = 5, message = "*The assignment type must have at least 5 characters")
    @Column(nullable = false)
    private String title;

    @Length(min = 5, message = "*The assignment type must have at least 5 characters")
    @Column(nullable = false)
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
