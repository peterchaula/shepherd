package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("adminHomeController")
public class HomeController extends AdminController {

    @GetMapping({"admin/home", "/admin"})
    public String index(UserService userService, Model model) {
        model.addAttribute("userName", "Welcome " + this.getAuthUser().getFirstName() + " " + this.getAuthUser().getLastName() + " (" + this.getAuthUser().getEmail() + ")");
        model.addAttribute("adminMessage", "Content Available Only for Users with Admin Role");

        return "redirect:/admin/company";
    }
}
