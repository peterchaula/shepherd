package io.github.peterchaula.shepherd.config;

import io.github.peterchaula.shepherd.components.ApiAuthenticationSuccessHandler;
import io.github.peterchaula.shepherd.components.JWTAuthenticationFilter;
import io.github.peterchaula.shepherd.components.JWTAuthorizationFilter;
import io.github.peterchaula.shepherd.components.AuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final DataSource dataSource;

    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    private final ApiAuthenticationSuccessHandler apiAuthenticationSuccessHandler;

    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Value("auth.jwt.secret")
    private String jwtSecret;


    @Autowired
    public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
                             DataSource dataSource,
                             AuthenticationSuccessHandler authenticationSuccessHandler, ApiAuthenticationSuccessHandler apiAuthenticationSuccessHandler) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.dataSource = dataSource;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.apiAuthenticationSuccessHandler = apiAuthenticationSuccessHandler;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/img/**", "/css/**", "/js/**").permitAll()
                .mvcMatchers("/register").permitAll()
                .mvcMatchers("/login").permitAll()
                .mvcMatchers("/api/v1/**").authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(), apiAuthenticationSuccessHandler))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtSecret))
                .authorizeRequests()
                .mvcMatchers("/admin/**").authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .successHandler(authenticationSuccessHandler)
                .usernameParameter("email")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }
}
