package io.github.peterchaula.shepherd.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
public class Company extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotEmpty(message = "{validation.name.notEmpty}")
    private String name;

    @Length(min = 5, message = "{validation.length}")
    private String phone;

    @NotEmpty(message = "{validation.notEmpty}")
    @Email(message = "{validation.email.valid}")
    private String email;

    @Column(name = "province_code", insertable = false, updatable = false)
    private String provinceCodeFk;

    @NotEmpty(message = "{validation.notEmpty}")
    @Length(min = 3, message = "{validation.length}")
    private String city;

    @Length(min = 3, message = "{validation.length}")
    private String street;

    @NotEmpty(message = "{validation.notEmpty}")
    @Length(min = 3, message = "{validation.length}")
    private String postalCode;

    @Column(nullable = false)
    private float lat;

    @Column(nullable = false)
    private float lon;

    @NotEmpty(message = "{validation.notEmpty}")
    @Length(min = 4, message = "{validation.length}")
    private String representative;

    @NotEmpty(message = "{validation.notEmpty}")
    @Length(min = 10, message = "{validation.length}")
    private String description;

    @ManyToOne
    @JoinColumn(name = "province_code", nullable = false)
    @NotNull
    private Province province;

    @OneToMany(mappedBy = "company")
    @JsonIgnore
    private Set<AssignmentType> assignmentTypes = new HashSet<>();

    @OneToMany(mappedBy = "company")
    @JsonIgnore
    private Set<Assignment> assignments = new HashSet<>();

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<User> users = new HashSet<>();

    @OneToMany(mappedBy = "company")
    @JsonIgnore
    private Set<VehicleType> vehicleTypes = new HashSet<>();

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Vehicle> vehicles = new HashSet<>();

    @OneToMany(mappedBy = "company")
    @JsonIgnore
    private Set<Address> addresses = new HashSet<>();
}
