
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.entity.Vehicle;
import io.github.peterchaula.shepherd.data.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleService {
    private final VehicleRepository vehicleRepository;
    private final CompanyService companyService;

    @Autowired
    public VehicleService(VehicleRepository vehicleRepository, CompanyService companyService) {
        this.vehicleRepository = vehicleRepository;
        this.companyService = companyService;
    }

    public void save(Vehicle vehicle) {
        //save bi-directional link
        vehicle.getCompany().getVehicles().add(vehicle);

        vehicleRepository.save(vehicle);
        companyService.save(vehicle.getCompany());
    }

    public List<Vehicle> findByCompany(Company company, Pageable pageable) {
        return vehicleRepository.findByCompany(company, pageable);
    }
}
