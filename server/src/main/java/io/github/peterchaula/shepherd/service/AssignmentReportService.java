
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.AssignmentCount;
import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.repository.AssignmentReportRepository;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class AssignmentReportService {
    private final AssignmentReportRepository assignmentReportRepository;

    public AssignmentReportService(AssignmentReportRepository assignmentReportRepository) {
        this.assignmentReportRepository = assignmentReportRepository;
    }

    public Map<String, Integer> getDailyCompletionReport(Company company, ZonedDateTime from, ZonedDateTime to) {
        final List<AssignmentCount> periods = assignmentReportRepository.getDailyCompletionReport(
                company,
                Date.from(from.toInstant()),
                Date.from(to.toInstant()),
                ZonedDateTime.now().getOffset().toString()
        );

        final Map<String, Integer> result = new TreeMap<>();

        for (AssignmentCount period : periods) {
            result.put(period.getPeriod(), period.getCount());
        }

        for (int i = 0; i <= Period.between(from.toLocalDate(), to.toLocalDate()).getDays(); i++) {
            final String period = from.plusDays(i).toLocalDate().toString();

            if (!result.containsKey(period)) {
                result.put(period, 0);
            }
        }

        return result;
    }

    public Map<String, Integer> getAnnualCompletionReport(Company company, ZonedDateTime from, ZonedDateTime to) {
        final List<AssignmentCount> periods = assignmentReportRepository.getAnnualCompletionReport(
                company,
                Date.from(from.toInstant()),
                Date.from(to.toInstant()),
                ZonedDateTime.now().getOffset().toString()
        );

        final Map<String, Integer> result = new TreeMap<>();

        for (AssignmentCount period : periods) {
            result.put(period.getPeriod(), period.getCount());
        }

        for (int i = 0; i <= Period.between(from.toLocalDate(), to.toLocalDate()).getMonths(); i++) {
            final String period = YearMonth.from(from.plusMonths(i)).toString();

            if (!result.containsKey(period)) {
                result.put(period, 0);
            }
        }

        return result;
    }
}
