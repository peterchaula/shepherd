package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Role;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.data.repository.RoleRepository;
import io.github.peterchaula.shepherd.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Service("userService")
public class UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void createUser(User user) {
        user.setActive(1);
        Role userRole = roleRepository.findByName("ADMIN");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));

        final User authUser = getAuthUser();

        if (authUser != null) {
            user.setUser(authUser);
        } else {
            //user created during registration
            user.setUser(user);
        }

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setConfirmPassword(user.getPassword());

        userRepository.save(user);
    }


    public void updateUser(User user, User existing) {
        Role userRole = roleRepository.findByName("ADMIN");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));

        user.setCreatedOn(existing.getCreatedOn());
        user.setLastLogin(existing.getLastLogin());

        final User authUser = getAuthUser();

        if (authUser != null) {
            user.setUser(authUser);
        } else {
            //user created during registration
            user.setUser(user);
        }

        //password update
        if (user.getPassword() != null && user.getPassword().length() > 0 && user.getPassword().equals(user.getConfirmPassword())) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        } else {
            user.setPassword(existing.getPassword());
        }

        userRepository.save(user);
    }

    public boolean validatePassword(User user, boolean existing) {
        //registration or new user
        if (!existing) {
            return user.getPassword() != null
                    && user.getPassword().length() >= 6 && user.getPassword().equals(user.getConfirmPassword());
        }

        return user.getPassword() == null || user.getPassword().isEmpty()
                || user.getPassword().length() >= 6 && user.getPassword().equals(user.getConfirmPassword());
    }

    public User getAuthUser() {
        final Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        if (principal instanceof UserDetails) {
            return findUserByEmail(((UserDetails) principal).getUsername());
        } else {
            return null;
        }
    }

    public User findById(int userId) {
        return userRepository.findById(userId)
                .get();
    }

    public List<User> findByCompanyId(int id) {
        return userRepository.findByCompanyId(id);
    }

    public void updateLastLogin(UserDetails principal) {
        final String email = principal.getUsername();
        final User user = findUserByEmail(email);

        user.setLastLogin(Date.from(Instant.now()));
        updateUser(user, user);
    }
}
