package io.github.peterchaula.shepherd.config;

import io.github.peterchaula.shepherd.components.AuthenticatedInterceptor;
import io.github.peterchaula.shepherd.components.CompanySetupInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    private final AuthenticatedInterceptor authenticatedInterceptor;

    private final CompanySetupInterceptor companySetupInterceptor;

    @Autowired
    public WebMvcConfig(AuthenticatedInterceptor authenticatedInterceptor, CompanySetupInterceptor companySetupInterceptor) {
        this.authenticatedInterceptor = authenticatedInterceptor;
        this.companySetupInterceptor = companySetupInterceptor;
    }

    @Override public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("auth/login");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticatedInterceptor).addPathPatterns("/register/**");
        registry.addInterceptor(companySetupInterceptor).addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/company/setup");
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");

        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean getValidator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());

        return bean;
    }
}
