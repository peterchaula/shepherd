
package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.AssignmentCount;
import io.github.peterchaula.shepherd.data.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface AssignmentReportRepository extends JpaRepository<Assignment, Integer> {

    @Query(value="SELECT DATE(CONVERT_TZ(ended_on,'+00:00', ?4)) AS period, COUNT(id) AS count FROM assignment" +
            " WHERE status = 2" + //completed
            " AND company_id = ?1" +
            " AND ended_on BETWEEN ?2 AND ?3" +
            " GROUP BY period", nativeQuery = true)
    List<AssignmentCount> getDailyCompletionReport(Company company, Date from, Date to, String timeZone);

    @Query(value="SELECT DATE_FORMAT(CONVERT_TZ(ended_on,'+00:00', ?4), '%Y-%m') AS period, COUNT(id) AS count FROM assignment" +
            " WHERE status = 2" + //completed
            " AND company_id = ?1" +
            " AND ended_on BETWEEN ?2 AND ?3" +
            " GROUP BY period", nativeQuery = true)
    List<AssignmentCount> getAnnualCompletionReport(Company company, Date from, Date to, String timeZone);
}
