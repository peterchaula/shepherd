
package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Address;
import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AddressRepository extends BaseRepository<Address, Integer> {
}
