package io.github.peterchaula.shepherd.components;

import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CompanySetupInterceptor implements HandlerInterceptor {

    @Autowired
    @Lazy
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        final Company company = userService.findUserByEmail(userDetails.getUsername()).getCompany();

        if (company == null) {
            response.sendRedirect("/admin/company/setup");
            return false;
        }

        return true;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
