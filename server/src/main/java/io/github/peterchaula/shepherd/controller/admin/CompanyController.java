package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.entity.Province;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.service.*;
import io.github.peterchaula.shepherd.session.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import static io.github.peterchaula.shepherd.data.entity.Assignment.*;
import static io.github.peterchaula.shepherd.data.entity.Assignment.Status.PENDING;

@Controller
@RequestMapping("admin/company")
public class CompanyController extends AdminController {

    private final CompanyService companyService;

    private final UserService userService;

    private final ProvinceService provinceService;

    private final AssignmentTypeService assignmentTypeService;

    private final VehicleTypeService vehicleTypeService;

    private final AssignmentService assignmentService;

    @Autowired
    public CompanyController(
            CompanyService companyService,
            UserService userService,
            ProvinceService provinceService,
            AssignmentTypeService assignmentTypeService,
            VehicleTypeService vehicleTypeService,
            AssignmentService assignmentService
    ) {
        this.companyService = companyService;
        this.userService = userService;
        this.provinceService = provinceService;
        this.assignmentTypeService = assignmentTypeService;
        this.vehicleTypeService = vehicleTypeService;
        this.assignmentService = assignmentService;
    }

    @GetMapping
    public String index(Model model) {
        final int companyId = getAuthUser().getCompany().getId();
        final List<Integer> statuses = Arrays.asList(PENDING.ordinal(), Status.STARTED.ordinal());

        model.addAttribute("assignmentTypes", assignmentTypeService.findByCompanyId(companyId))
                .addAttribute("assignments", assignmentService.findByCompanyAndStatusIn(getAuthUser().getCompany(), statuses))
                .addAttribute("company", getAuthUser().getCompany());

        return "admin/company/index";
    }

    @PostMapping("/setup")
    @Transactional
    public String store(@Valid Company company, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "admin/company/setup";
        }

        final User authUser = getAuthUser();

        //users are not supposed to have more than one company
        if (authUser.getCompany() != null) {
            return "redirect:/admin/company";
        }

        company.setUser(authUser);
        companyService.save(company);

        authUser.setCompany(company);
        authUser.setUser(authUser);
        userService.updateUser(authUser, authUser);

        //add default type
        assignmentTypeService.addDefault(authUser);
        vehicleTypeService.addDefaults(authUser);


        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "You have successfully setup your company!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company";
    }

    @GetMapping("/edit")
    public String edit(Model model) {
        model.addAttribute("company", getAuthUser().getCompany());

        return "admin/company/edit";
    }


    @PutMapping
    @Transactional
    public String update(@Valid Company company, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("company", company);

            return "admin/company/edit";
        }

        final User authUser = getAuthUser();

        company.setId(getAuthUser().getCompany().getId());
        company.setUser(authUser);
        companyService.save(company);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "You have successfully updated your company!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company";
    }

    @GetMapping("/setup")
    public String setup(Model model) {
        model.addAttribute("company", new Company());

        return "admin/company/setup";
    }

    @ModelAttribute("provinces")
    public Iterable<Province> getProvinces() {
        return provinceService.findAll();
    }
}
