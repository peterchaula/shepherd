
package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AssignmentRepository extends PagingAndSortingRepository<Assignment, Integer> {
    List<Assignment> findByCompany(Company company, Pageable pageable);

    @Query("SELECT a FROM Assignment a" +
            " WHERE a.company = ?1" +
            " AND a.status IN(?2)" +
            " AND  (" +
            " a.reference LIKE %?3%" +
            " OR a.description LIKE %?3%" +
            " OR a.driver IS NOT NULL AND EXISTS (FROM User u WHERE u = a.driver AND u.firstName LIKE %?3%)"+
            " OR a.driver IS NOT NULL AND EXISTS (FROM User u WHERE u = a.driver AND u.firstName LIKE %?3%)"+
            " OR EXISTS ( FROM  Address fa WHERE a.fromAddress = fa AND fa.street LIKE %?3% )" +
            " OR EXISTS ( FROM  Address fa WHERE a.fromAddress = fa AND fa.suburb LIKE %?3% )" +
            " OR EXISTS ( FROM  Address fa WHERE a.fromAddress = fa AND fa.city LIKE %?3% )" +
            " OR EXISTS ( FROM  Address fa WHERE a.fromAddress = fa AND fa.postalCode LIKE %?3% )" +
            " OR EXISTS ( FROM  Address ta WHERE a.toAddress = ta AND  ta.street LIKE %?3% )" +
            " OR EXISTS ( FROM  Address ta WHERE a.toAddress = ta AND  ta.suburb LIKE %?3% )" +
            " OR EXISTS ( FROM  Address ta WHERE a.toAddress = ta AND  ta.city LIKE %?3% )" +
            " OR EXISTS ( FROM  Address ta WHERE a.toAddress = ta AND  ta.postalCode LIKE %?3% )" +
            ")"
    )
    List<Assignment> search(
            Company company,
            @Param("statuses") List<Assignment.Status> statuses,
            @Param("search") String search,
            Pageable pageable
    );

    List<Assignment> findByCompanyAndStatusIn(Company company, List<Assignment.Status> statuses);

    List<Assignment> findByDriverAndIdNotInAndStatusIn(
            User driver,
            List<Integer> assignmentIds,
            List<Assignment.Status> statuses
    );

    List<Assignment> findByDriverAndStatusIn(User user, List<Assignment.Status> statuses);

    List<Assignment> findByIdIn(List<Integer> assignmentIds);
}
