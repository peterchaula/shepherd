package io.github.peterchaula.shepherd.controller.api;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.service.AssignmentService;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController("assignmentApiController")
@RequestMapping("/api/v1/company/assignment")
public class AssignmentController extends ApiController {
    private final AssignmentService assignmentService;
    private final UserService userService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService, UserService userService) {
        this.assignmentService = assignmentService;
        this.userService = userService;
    }

    @GetMapping
    public List<Assignment> index(@RequestParam(required = false) List<Integer> statuses) {
        return assignmentService.findByCompanyAndStatusIn(
                getAuthUser().getCompany(),
                Optional.ofNullable(statuses).orElse(Collections.emptyList())
        );
    }

    @PutMapping("user/{userId}/manifest")
    @ResponseBody
    public List<Assignment> updateManifest(@PathVariable int userId, @NotEmpty @RequestBody List<@Valid Assignment> assignments) {
        return assignmentService.updateManifest(userService.findById(userId), assignments);
    }

    @GetMapping("{id}")
    public Assignment retrieve(@PathVariable int id) {
        return assignmentService.findByIdIn(id);
    }

    @PatchMapping("{id}/start")
    public Assignment start(@PathVariable int id) {
        Assignment assignment = assignmentService.findByIdIn(id);
        assignmentService.start(assignment, getAuthUser());
        assignmentService.save(assignment);

        return assignmentService.findByIdIn(id);
    }

    @PatchMapping("{id}/complete")
    public Assignment complete(@PathVariable int id) {
        Assignment assignment = assignmentService.findByIdIn(id);
        assignmentService.complete(assignment, getAuthUser());
        assignmentService.save(assignment);

        return assignmentService.findByIdIn(id);
    }

    @PatchMapping("{id}/cancel")
    public Assignment cancel(@PathVariable int id) {
        Assignment assignment = assignmentService.findByIdIn(id);
        assignmentService.cancel(assignment, getAuthUser());
        assignmentService.save(assignment);

        return assignmentService.findByIdIn(id);
    }
}
