package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.Address;
import io.github.peterchaula.shepherd.data.entity.Province;
import io.github.peterchaula.shepherd.service.AddressService;
import io.github.peterchaula.shepherd.service.ProvinceService;
import io.github.peterchaula.shepherd.session.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/company/address")
public class AddressController extends AdminController {

    private AddressService addressService;
    private ProvinceService provinceService;

    @Autowired
    public AddressController(AddressService addressService, ProvinceService provinceService) {
        this.addressService = addressService;
        this.provinceService = provinceService;
    }

    @GetMapping
    public String index(Model model, Pageable pageable) {
        model.addAttribute("addresses", addressService.getCompanyAddresses(getAuthUser().getCompany()))
                .addAttribute("pageable", pageable);

        return "admin/address/index";
    }

    @GetMapping("create")
    public String create(Model model) {
        model.addAttribute("address", new Address());

        return "admin/address/create";
    }

    @PostMapping
    public String store(@Valid Address address, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("address", address);

            return "admin/address/create";
        }

        address.setCompany(getAuthUser().getCompany());
        address.setUser(getAuthUser());
        addressService.save(address);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Address successfully created!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/address";
    }

    @GetMapping("{id}/edit")
    public String edit(@PathVariable("id") int id, Model model) {
        model.addAttribute("address", addressService.findById(id));

        return "admin/address/edit";
    }

    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String update(@PathVariable int id, @Valid Address address, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("address", address);

            return "admin/address/edit";
        }

        address.setId(id);
        address.setCompany(getAuthUser().getCompany());
        address.setUser(getAuthUser());
        addressService.save(address);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Address successfully updated!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/address";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") int id, RedirectAttributes redirectAttributes) {
        final Address address = addressService.findById(id);

        addressService.delete(address);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "Address successfully deleted!",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/address";
    }

    @ModelAttribute("provinces")
    public Iterable<Province> getProvinces() {
        return provinceService.findAll();
    }
}
