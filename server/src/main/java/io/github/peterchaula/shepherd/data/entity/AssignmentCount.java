package io.github.peterchaula.shepherd.data.entity;

public interface AssignmentCount {
    String getPeriod();

    int getCount();
}
