package io.github.peterchaula.shepherd.data.repository;


import io.github.peterchaula.shepherd.data.entity.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Integer> {
}
