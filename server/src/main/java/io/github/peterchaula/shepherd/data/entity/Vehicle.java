package io.github.peterchaula.shepherd.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@ToString
@Entity
@JsonIgnoreProperties({"user", "company"})
public class Vehicle extends BaseEntity {
    @Length(min = 2, max = 30)
    private String make;

    @Length(min = 2, max = 30)
    @Column(nullable = false)
    private String model;

    @Min(1950)
    @Column(nullable = false)
    private short year;

    @Length(min = 3, max = 20)
    @Column(nullable = false)
    private String registration;

    @Length(min = 5, max = 30)
    @Column(nullable = false)
    private String engineNumber;

    @Length(min = 5, max = 30)
    @Column(nullable = false)
    private String vinNumber;

    @Length(min = 2, max = 15)
    @Column(nullable = false)
    private String colour = "";

    @Min(1)
    @Max(2)
    @Column(nullable = false)
    private byte fuelType;

    @Min(1)
    @Column(nullable = false)
    private float fuelTankCapacity;

    @Min(1)
    @Column(nullable = false)
    private int nvm;

    @Min(1)
    @Column(nullable = false)
    private int gvm;

    @Min(0)
    @Column(nullable = false)
    private int odometerReading;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false)
    private Date licenceExpiresOn;

    @OneToOne
    @JoinColumn(name = "type_id", nullable = false)
    @NotNull
    private VehicleType type;

    enum FuelType {
        PETROL("Petrol"),
        DIESEL("Diesel");

        public final String name;

        FuelType(String name) {
            this.name = name;
        }
    }
}
