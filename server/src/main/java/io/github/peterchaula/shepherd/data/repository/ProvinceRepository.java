package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Province;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvinceRepository extends CrudRepository<Province, String> {
}
