
package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.entity.Vehicle;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VehicleRepository extends CrudRepository<Vehicle, Integer> {
    List<Vehicle> findByCompany(Company company, Pageable pageable);
}
