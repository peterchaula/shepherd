
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Role;
import io.github.peterchaula.shepherd.data.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(@Autowired  RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    public void save(Role role){
        this.roleRepository.save(role);
    }
}
