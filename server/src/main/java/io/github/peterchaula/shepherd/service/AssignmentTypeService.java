package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.AssignmentType;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.data.repository.AssignmentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignmentTypeService {
    private final AssignmentTypeRepository assignmentTypeRepository;

    public AssignmentTypeService(@Autowired  AssignmentTypeRepository assignmentTypeRepository){
        this.assignmentTypeRepository = assignmentTypeRepository;
    }

    public void save(AssignmentType assignmentType){
        this.assignmentTypeRepository.save(assignmentType);
    }

    public void addDefault(User user) {
        AssignmentType type = new AssignmentType();
        type.setTitle("Default");
        type.setDescription("Default");
        type.setCompany(user.getCompany());
        type.setUser(user);

        assignmentTypeRepository.save(type);
    }

    public List<AssignmentType> findByCompanyId(int id) {
        return assignmentTypeRepository.findByCompanyId(id);
    }
}
