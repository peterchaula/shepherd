
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Address;
import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {
    private final AddressRepository addressRepository;

    public AddressService(@Autowired AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public void save(Address address) {
        this.addressRepository.save(address);
    }

    public List<Address> getCompanyAddresses(Company company) {
        return addressRepository.findByCompanyAndDeletedOnIsNull(company);
    }

    public Address findById(int id) {
        return addressRepository.findById(id).get();
    }

    public void delete(Address address) {
        addressRepository.delete(address);
    }
}
