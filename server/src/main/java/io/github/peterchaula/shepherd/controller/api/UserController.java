package io.github.peterchaula.shepherd.controller.api;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("userApiController")
@RequestMapping("/api/v1/company/user")
public class UserController extends ApiController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> index() {
        return userService.findByCompanyId(getAuthUser().getCompany().getId());
    }
}
