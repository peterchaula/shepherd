package io.github.peterchaula.shepherd.controller.api;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.AssignmentCount;
import io.github.peterchaula.shepherd.service.AssignmentReportService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

@RestController("assignmentReportApiController")
@RequestMapping("/api/v1/company/assignment/report")
public class AssignmentReportController extends ApiController {
    private final AssignmentReportService assignmentReportService;

    public AssignmentReportController(AssignmentReportService assignmentReportService) {
        this.assignmentReportService = assignmentReportService;
    }

    @GetMapping("weekly")
    public Map<String, Integer> weekly() {
        final ZonedDateTime midnight = ZonedDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT, ZoneId.systemDefault());
        final ZonedDateTime eofDay = ZonedDateTime.of(LocalDate.now(), LocalTime.MAX, ZoneId.systemDefault());

        return assignmentReportService.getDailyCompletionReport(
                getAuthUser().getCompany(),
                midnight.with(ChronoField.DAY_OF_WEEK, DayOfWeek.MONDAY.getValue()),
                eofDay.with(ChronoField.DAY_OF_WEEK, DayOfWeek.SUNDAY.getValue())
        );
    }

    @GetMapping("monthly")
    public Map<String, Integer> monthly() {
        final LocalDate now = LocalDate.now();
        final ZoneId tz = ZoneId.systemDefault();

        return assignmentReportService.getDailyCompletionReport(
                getAuthUser().getCompany(),
                ZonedDateTime.of(now.withDayOfMonth(1), LocalTime.MIDNIGHT, tz),
                ZonedDateTime.of(now.with(lastDayOfMonth()), LocalTime.MAX, tz)
        );
    }

    @GetMapping("annual")
    public Map<String, Integer> annual() {
        final LocalDate now = LocalDate.now();
        final ZoneId tz = ZoneId.systemDefault();

        return assignmentReportService.getAnnualCompletionReport(
                getAuthUser().getCompany(),
                ZonedDateTime.of(now.withMonth(1).withDayOfMonth(1), LocalTime.MIDNIGHT, tz),
                ZonedDateTime.of(now.withMonth(12).with(lastDayOfMonth()), LocalTime.MAX, tz)
        );
    }
}
