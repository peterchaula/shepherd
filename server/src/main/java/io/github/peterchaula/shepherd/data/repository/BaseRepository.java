package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.BaseEntity;
import io.github.peterchaula.shepherd.data.entity.Company;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity, Integer> extends CrudRepository<T, Integer> {
    @Query("UPDATE #{#entityName} e SET deletedOn = CURRENT_TIMESTAMP WHERE e.id = :id")
    @Override
    void deleteById(Integer id);

    @Transactional
    @Modifying
    @Query("UPDATE #{#entityName} e SET e.deletedOn = CURRENT_TIMESTAMP WHERE e.id = ?#{#entity.id}")
    @Override
    void delete(@Param("entity") T entity);

    List<T> findByCompanyAndDeletedOnIsNull(Company company);
}
