package io.github.peterchaula.shepherd.controller.api;

import io.github.peterchaula.shepherd.data.entity.Address;
import io.github.peterchaula.shepherd.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController("addressApiController")
@RequestMapping("/api/v1/company/address")
public class AddressController extends ApiController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Address update(@PathVariable int id, @RequestBody @Valid Address address) {

        address.setId(id);
        address.setCompany(getAuthUser().getCompany());
        address.setUser(getAuthUser());
        addressService.save(address);

        return addressService.findById(address.getId());
    }
}
