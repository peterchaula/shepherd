package io.github.peterchaula.shepherd.data.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@ToString
@Entity
public class Province {
    @Id
    @Length(min = 3, max = 3)
    private String code;

    @Length(min = 3)
    private String name;
}
