package io.github.peterchaula.shepherd.controller;


import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    private UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/register")
    public String register(Model model) {
        model.addAttribute("user", new User());

        return "auth/register";
    }

    @PostMapping(value = "/register")
    public String store(@Valid User user, BindingResult bindingResult) {
        if (userService.findUserByEmail(user.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.user", "Email already taken!");
        }

        if (!userService.validatePassword(user, false)) {
            bindingResult.rejectValue("password", "", "Passwords should be at least 6 characters.");
        }

        if (bindingResult.hasErrors()) {
            return "auth/register";
        } else {
            userService.createUser(user);

            return "redirect:/login?register";
        }
    }
}
