package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Province;
import io.github.peterchaula.shepherd.data.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvinceService{
    private ProvinceRepository provinceRepository;

    @Autowired
    public ProvinceService(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    public Iterable<Province> findAll(){
        return provinceRepository.findAll();
    }
}
