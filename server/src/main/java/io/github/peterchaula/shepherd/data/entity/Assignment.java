package io.github.peterchaula.shepherd.data.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
//@ToString
@Entity
public class Assignment extends BaseEntity {

    public enum Status {
        PENDING,
        STARTED,
        COMPLETED,
        CANCELLED;

        public static Status parse(int status) {
            for (Status s : values()) {
                if (s.ordinal() == status) {
                    return s;
                }
            }

            throw new IllegalArgumentException("Invalid status");
        }
    }

    @Length(min = 5, message = "*The assignment  requires a substantial description")
    @NotNull
    private String description;

    @Enumerated
    @NotNull
    private Status status = Status.PENDING;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date startingOn = Calendar.getInstance().getTime();

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endingOn = Calendar.getInstance().getTime();

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endedOn;

    private String reference;

    @ManyToOne
    @JoinColumn(name = "driver_id", columnDefinition = "INT DEFAULT 0")
    @Nullable
    private User driver;

    @Column(columnDefinition = "INT DEFAULT 0")
    private int sequence;

    @ManyToOne
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "assignment_type_id", nullable = false)
    private AssignmentType type;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "from_address_id", nullable = false)
    private Address fromAddress;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "to_address_id", nullable = false)
    private Address toAddress;
}
