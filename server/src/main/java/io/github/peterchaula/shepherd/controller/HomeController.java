package io.github.peterchaula.shepherd.controller;

import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("websiteHomeController")
public class HomeController {

    @GetMapping({"/"})
    public String index(UserService userService, Model model) {
        return "home/index";
    }
}
