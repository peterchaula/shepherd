
package io.github.peterchaula.shepherd.data.repository;

import io.github.peterchaula.shepherd.data.entity.VehicleType;
import org.springframework.data.repository.CrudRepository;

public interface VehicleTypeRepository extends CrudRepository<VehicleType, Integer> {
}
