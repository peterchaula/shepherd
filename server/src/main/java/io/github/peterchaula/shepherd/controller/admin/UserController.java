package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.controller.AdminController;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.service.UserService;
import io.github.peterchaula.shepherd.session.FlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/company/user")
public class UserController extends AdminController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("users", getAuthUser().getCompany().getUsers());

        return "admin/user/index";
    }

    @GetMapping("create")
    public String create(Model model) {
        User user = new User();
        user.setTermsAccepted(true);
        user.setCompany(getAuthUser().getCompany());

        model.addAttribute("user", user);

        return "admin/user/create";
    }

    @PostMapping
    public String store(@Valid User user, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        User authUser = getAuthUser();

        if (!authUser.getCompany().equals(user.getCompany())) {
            bindingResult.rejectValue("company", "", "Invalid company!");
        }

        //enforce unique constraint on email
        if (userService.findUserByEmail(user.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.user", "Email already taken!");
        }

        if (!userService.validatePassword(user, false)) {
            bindingResult.rejectValue("password", "", "Passwords should be at least 6 characters and should match");
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);

            return "admin/user/create";
        }

        userService.createUser(user);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "User successfully added",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/user";
    }

    @GetMapping("{id}/edit")
    public String edit(@PathVariable("id") int id, Model model) {
        model.addAttribute("user", userService.findById(id));

        return "admin/user/edit";
    }


    @PutMapping("{id}")
    public String update(@PathVariable("id") int id, @Valid User user, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (!getAuthUser().getCompany().equals(user.getCompany())) {
            bindingResult.rejectValue("company", "", "Invalid company!");
        }

        final User existingUser = userService.findById(id);

        if (!existingUser.getEmail().equals(user.getEmail())) {
            bindingResult.rejectValue("email", "error.user", "Email cannot be modified");
        }

        if (!userService.validatePassword(user, true)) {
            bindingResult.rejectValue("password", "", "Passwords should be at least 6 characters and should match");
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);

            return "admin/user/edit";
        }

        userService.updateUser(user, existingUser);

        redirectAttributes.addFlashAttribute("message", new FlashMessage(
                "User successfully updated",
                FlashMessage.STATUS.SUCCESS
        ));

        return "redirect:/admin/company/user";
    }

    @GetMapping("tracking")
    public String tracking() {

        return "admin/user/tracking";
    }
}
