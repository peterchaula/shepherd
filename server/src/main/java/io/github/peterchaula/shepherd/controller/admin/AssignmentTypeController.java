package io.github.peterchaula.shepherd.controller.admin;

import io.github.peterchaula.shepherd.service.AssignmentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AssignmentTypeController {
    final AssignmentTypeService assignmentTypeService;

    @Autowired
    public AssignmentTypeController(AssignmentTypeService assignmentTypeService) {
        this.assignmentTypeService = assignmentTypeService;
    }
}
