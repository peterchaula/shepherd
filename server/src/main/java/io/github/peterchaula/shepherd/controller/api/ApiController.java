package io.github.peterchaula.shepherd.controller.api;

import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.transaction.Transactional;

/**
 * Not a very clever design
 */
@Transactional
public class ApiController {
    @Autowired
    private UserService userService;

    @ModelAttribute("authUser")
    public User getAuthUser() {
        return userService.getAuthUser();
    }
}
