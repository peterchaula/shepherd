package io.github.peterchaula.shepherd.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@JsonIgnoreProperties(value = {"assignments", "company", "user", "password"})
public class User extends BaseEntity{
    @Email(message = "{validation.email.valid}")
    @NotEmpty(message = "{validation.email.notEmpty}")
    @Column(unique = true)
    private String email;

    private String password;

    @Transient
    private String confirmPassword;

    @NotEmpty(message = "{validation.name.first.notEmpty}")
    private String firstName;

    private String lastName;

    @ColumnDefault("0")
    private int active;

    @ColumnDefault("0")
    private double latitude;

    @ColumnDefault("0")
    private double longitude;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;

    private String phone;

    @AssertTrue(message = "{validation.terms.accepted}")
    private boolean termsAccepted;

    @OneToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @OneToMany(mappedBy = "driver")
    private List<Assignment> assignments = new ArrayList<>();

    @JsonProperty
    public String getFullName(){
        return  this.firstName + " " + this.lastName;
    }
}
