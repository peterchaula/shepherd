package io.github.peterchaula.shepherd.session;

public class FlashMessage {

    public enum STATUS {
        SUCCESS("success"),
        INFO("info"),
        ERROR("error");

        public final String code;

        STATUS(String code) {
            this.code = code;
        }

        @Override public String toString() {
            return code;
        }
    }

    private final String message;

    public final STATUS status;

    public FlashMessage(String message, STATUS status) {
        this.message = message;
        this.status = status;
    }

    @Override public String toString() {
        return message;
    }
}
