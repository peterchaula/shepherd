
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.data.entity.VehicleType;
import io.github.peterchaula.shepherd.data.repository.VehicleTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleTypeService {
    private final VehicleTypeRepository vehicleTypeRepository;

    public VehicleTypeService(@Autowired  VehicleTypeRepository vehicleTypeRepository){
        this.vehicleTypeRepository = vehicleTypeRepository;
    }

    public void save(VehicleType vehicleType){
        this.vehicleTypeRepository.save(vehicleType);
    }

    public void addDefaults(User user) {
        final String[][] types = {
                {"Bicycle", "Bicycle"},
                {"Motorcycle", "Motorcycle"},
                {"Pickup", "Pickup Truck"},
                {"Van", "Van"},
                {"Lorry", "Lorry"},
                {"Truck", "Truck"},
        };

        for (String[] type: types) {
            final VehicleType vehicleType = new VehicleType();
            vehicleType.setName(type[0]);
            vehicleType.setDescription(type[1]);
            vehicleType.setUser(user);
            vehicleType.setCompany(user.getCompany());

            vehicleTypeRepository.save(vehicleType);
        }
    }
}
