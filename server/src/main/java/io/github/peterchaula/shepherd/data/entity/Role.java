package io.github.peterchaula.shepherd.data.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;

@Getter
@Setter
@ToString
@Entity
public class Role extends BaseEntity{
    private String name;
    private String description;
}
