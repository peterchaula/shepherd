
package io.github.peterchaula.shepherd.service;

import io.github.peterchaula.shepherd.data.entity.Assignment;
import io.github.peterchaula.shepherd.data.entity.Company;
import io.github.peterchaula.shepherd.data.entity.User;
import io.github.peterchaula.shepherd.data.repository.AssignmentRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AssignmentService {
    private AssignmentRepository assignmentRepository;

    public AssignmentService(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    public void save(Assignment assignment) {
        assignmentRepository.save(assignment);
    }

    public List<Assignment> findAll(Company company, Pageable pageable) {
        return assignmentRepository.findByCompany(company, pageable);
    }

    public Assignment findByIdIn(int id) {
        final Optional<Assignment> assignment = assignmentRepository.findById(id);

        if (assignment.isPresent()) {
            return assignment.get();
        }

        throw new EntityNotFoundException("Assignment " + id + " not found");
    }

    public void start(Assignment assignment, User user) {
        assignment.setStatus(Assignment.Status.STARTED);
        assignment.setUser(user);
        assignment.setEndedOn(Calendar.getInstance().getTime());
    }

    public void complete(Assignment assignment, User user) {
        assignment.setStatus(Assignment.Status.COMPLETED);
        assignment.setUser(user);
        assignment.setEndedOn(Calendar.getInstance().getTime());
    }

    public void cancel(Assignment assignment, User user) {
        assignment.setStatus(Assignment.Status.CANCELLED);
        assignment.setUser(user);
        assignment.setEndedOn(Calendar.getInstance().getTime());
    }

    public List<Assignment> search(Company company, List<Integer> statuses, String search, Pageable pageable) {
        List<Assignment.Status> statusList = Arrays.asList(Assignment.Status.values());

        if (!statuses.isEmpty()) {
            statusList = statuses.stream()
                    .map(Assignment.Status::parse)
                    .collect(Collectors.toList());
        }

        return assignmentRepository.search(company, statusList, search, pageable);
    }

    public List<Assignment> findByCompanyAndStatusIn(Company company, List<Integer> statuses) {
        List<Assignment.Status> statusList = Arrays.asList(Assignment.Status.values());

        if (!statuses.isEmpty()) {
            statusList = statuses.stream()
                    .map(Assignment.Status::parse)
                    .collect(Collectors.toList());
        }

        return assignmentRepository.findByCompanyAndStatusIn(company, statusList);
    }

    public List<Assignment> updateManifest(User user, List<Assignment> assignments) {
        final List<Assignment.Status> statuses = Arrays.asList(Assignment.Status.PENDING, Assignment.Status.STARTED);
        final List<Integer> assignmentIds = Stream.concat(Stream.of(-1), assignments.stream().map(Assignment::getId))
                .collect(Collectors.toList());

        final List<Assignment> unassigned = assignmentRepository.findByDriverAndIdNotInAndStatusIn(user, assignmentIds, statuses);

        for (Assignment assignment : unassigned) {
            assignment.setSequence(0);
            assignment.setDriver(null);
            save(assignment);
        }

        for (Assignment assignment : assignments) {
            assignment.setDriver(user);
            assignment.setSequence(assignments.indexOf(assignment));
            assignment.setStatus(Assignment.Status.STARTED);
            save(assignment);
        }

        return assignmentRepository.findByDriverAndStatusIn(user, statuses);
    }

    public List<Assignment> findByIdIn(List<Integer> assignmentIds) {
        return assignmentRepository.findByIdIn(assignmentIds);
    }
}
